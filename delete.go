package kubectl

import (
	"fmt"
	"strings"

	"gitlab.com/digitalxero/simple-actions"
)

type deleteAction struct {
	namespace,
	filepath string
	labels    []string
	kustomize bool
}

// NewDeleteAction docs
func NewDeleteAction(namespace, filepath string, labels []string, kustomize bool) actions.Action {
	return &deleteAction{
		namespace: namespace,
		filepath:  filepath,
		labels:    labels,
		kustomize: kustomize,
	}
}

// Execute docs
func (a *deleteAction) Execute(ctx actions.ActionContext) (err error) {
	defer func() {
		ctx.Status().End(err == nil)
	}()
	ctx.Status().Start(fmt.Sprintf("Deleting %s", a.filepath))
	if ctx.IsDryRun() {
		return nil
	}

	args := []string{"delete"}

	if a.namespace != "" {
		args = append(args, "-n", a.namespace)
	}
	if a.labels != nil {
		for _, label := range a.labels {
			args = append(args, "-l", label)
		}
	}

	if a.kustomize {
		args = append(args, "-k")
	} else {
		args = append(args, "-f")
	}

	args = append(args, a.filepath)

	if err = execCmd(ctx.Logger(), args); err != nil && strings.Contains(err.Error(), "Error from server (NotFound)") {
		// if the container does not exist, then it is obviously removed.
		ctx.Logger().V(3).Info(err.Error())
		err = nil
	}

	return err
}

type deleteResourceAction struct {
	namespace,
	resource,
	name string
	all bool
}

// NewDeleteResourceAction docs
func NewDeleteResourceAction(namespace, resource, name string, all bool) actions.Action {
	return &deleteResourceAction{
		namespace: namespace,
		resource:  resource,
		name:      name,
		all:       all,
	}
}

// Execute docs
func (a *deleteResourceAction) Execute(ctx actions.ActionContext) (err error) {
	defer func() {
		ctx.Status().End(err == nil)
	}()
	ctx.Status().Start(fmt.Sprintf("Deleting %s %s", a.resource, a.name))
	if ctx.IsDryRun() {
		return nil
	}

	args := []string{"delete", a.resource}

	if a.all {
		args = append(args, "--all")
	} else {
		args = append(args, a.name)
	}

	if a.namespace != "" {
		args = append(args, "-n", a.namespace)
	}

	err = execCmd(ctx.Logger(), args)

	return err
}
