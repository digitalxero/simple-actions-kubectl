// Package kubectl contains kubectl related actions
package kubectl

import (
	"os/exec"

	"github.com/pkg/errors"

	"gitlab.com/digitalxero/simple-actions/log"
)

func execCmd(logger log.Logger, args []string) (err error) {
	var out []byte
	// nolint:gosec
	cmd := exec.Command("kubectl", args...)
	logger.V(3).Info(cmd.String())
	if out, err = cmd.CombinedOutput(); err != nil {
		return errors.Wrap(err, string(out))
	}

	logger.V(3).Info(string(out))

	return nil
}
