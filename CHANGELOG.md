

<!--- next entry here -->

## 1.1.2
2021-02-25

### Fixes

- update deps (e5dd4b08de9536ba48649b0074f7b6312e262f70)

## 1.1.1
2021-02-25

### Fixes

- update deps (3b1c484474bbd15a4f12cb0281b62c1d3b9883ac)
- remove local replace (71b9581381195042f7afea16f51b83db89beda76)
- get annotate working (80262a307c7145c23ad3d6f8630df5c8bb3899f6)

## 1.0.0
2020-05-27

### Fixes

- initial release (5346f6ca53690bad568a4610076f763b921d1dd9)
- dep update (1b3d886f98109c717014b92abfe80eff875f18b3)
- module path in go.mod (4eb82dbb3cd24ff851bb8a1c2b4baa6a13b06778)
- Merge branch 'master' of gtlab.com:digitalxero/simple-actions-kubectl (0b874df0160224c62ba00499684cff90c7b4277a)

## 1.0.0
2020-05-27

### Fixes

- initial release (5346f6ca53690bad568a4610076f763b921d1dd9)
- dep update (1b3d886f98109c717014b92abfe80eff875f18b3)

## 1.0.0
2020-05-26

### Fixes

- initial release (5346f6ca53690bad568a4610076f763b921d1dd9)

