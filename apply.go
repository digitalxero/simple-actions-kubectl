package kubectl

import (
	"fmt"

	"gitlab.com/digitalxero/simple-actions"
)

type applyAction struct {
	namespace,
	filepath string
	labels    []string
	kustomize bool
}

// NewApplyAction docs
func NewApplyAction(namespace, filepath string, labels []string, kustomize bool) actions.Action {
	return &applyAction{
		namespace: namespace,
		filepath:  filepath,
		labels:    labels,
		kustomize: kustomize,
	}
}

// Execute docs
func (a *applyAction) Execute(ctx actions.ActionContext) (err error) {
	defer func() {
		ctx.Status().End(err == nil)
	}()
	ctx.Status().Start(fmt.Sprintf("Applying %s", a.filepath))
	if ctx.IsDryRun() {
		return nil
	}

	args := []string{"apply"}

	if a.namespace != "" {
		args = append(args, "-n", a.namespace)
	}
	if a.labels != nil {
		for _, label := range a.labels {
			args = append(args, "-l", label)
		}
	}

	if a.kustomize {
		args = append(args, "-k")
	} else {
		args = append(args, "-f")
	}

	args = append(args, a.filepath)

	err = execCmd(ctx.Logger(), args)

	return err
}
