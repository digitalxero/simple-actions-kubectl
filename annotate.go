package kubectl

import (
	"fmt"

	"gitlab.com/digitalxero/simple-actions"
)

type annotateAction struct {
	namespace,
	resource,
	key,
	value string
}

// NewAnnotateAction docs
func NewAnnotateAction(namespace, resource, key, value string) actions.Action {
	return &annotateAction{
		namespace: namespace,
		resource:  resource,
		key:       key,
		value:     value,
	}
}

// Execute docs
func (a *annotateAction) Execute(ctx actions.ActionContext) (err error) {
	defer func() {
		ctx.Status().End(err == nil)
	}()
	ctx.Status().Start(fmt.Sprintf("Annotating %s with %s=%s", a.resource, a.key, a.value))
	if ctx.IsDryRun() {
		return nil
	}

	args := []string{"annotate", a.resource, fmt.Sprintf("%s=%s", a.key, a.value)}

	if a.namespace != "" {
		args = append(args, "-n", a.namespace)
	}

	err = execCmd(ctx.Logger(), args)

	return err
}
