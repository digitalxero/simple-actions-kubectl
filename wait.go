package kubectl

import (
	"fmt"
	"time"

	"gitlab.com/digitalxero/simple-actions"
)

type waitAction struct {
	namespace,
	condition,
	resource,
	timeout string
	all bool
}

// NewWaitAction docs
func NewWaitAction(namespace, condition, resource, timeout string, all bool) actions.Action {
	return &waitAction{
		namespace: namespace,
		condition: condition,
		resource:  resource,
		timeout:   timeout,
		all:       all,
	}
}

// Execute docs
func (a *waitAction) Execute(ctx actions.ActionContext) (err error) {
	startTime := time.Now()
	defer func() {
		ctx.Status().End(err == nil)
		if err == nil {
			fmt.Printf(" • %s %s after %s 💚\n", a.resource, a.condition, actions.FormatDuration(time.Since(startTime)))
		}
	}()

	ctx.Status().Start(fmt.Sprintf("Waiting ≤ %s for %s %s", a.timeout, a.resource, a.condition))
	if ctx.IsDryRun() {
		return nil
	}

	args := []string{"wait", fmt.Sprintf("--for=condition=%s", a.condition), a.resource}
	if a.namespace != "" {
		args = append(args, "-n", a.namespace)
	}
	if a.timeout != "" {
		args = append(args, "--timeout", a.timeout)
	}
	if a.all {
		args = append(args, "--all")
	}

	err = execCmd(ctx.Logger(), args)

	return err
}
