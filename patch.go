package kubectl

import (
	"fmt"

	"gitlab.com/digitalxero/simple-actions"
)

type patchAction struct {
	namespace,
	resource,
	name,
	patch string
}

// NewPatchAction docs
func NewPatchAction(namespace, resource, name, patch string) actions.Action {
	return &patchAction{
		namespace: namespace,
		resource:  resource,
		name:      name,
		patch:     patch,
	}
}

// Execute docs
func (a *patchAction) Execute(ctx actions.ActionContext) (err error) {
	defer func() {
		ctx.Status().End(err == nil)
	}()
	ctx.Status().Start(fmt.Sprintf("Patching %s %s", a.resource, a.name))
	if ctx.IsDryRun() {
		return nil
	}

	args := []string{"patch", a.resource, a.name, "-p", a.patch}

	if a.namespace != "" {
		args = append(args, "-n", a.namespace)
	}

	err = execCmd(ctx.Logger(), args)

	return err
}
