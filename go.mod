module gitlab.com/digitalxero/simple-actions-kubectl

go 1.13

require (
	github.com/pkg/errors v0.9.1
	gitlab.com/digitalxero/simple-actions v1.5.0
)
