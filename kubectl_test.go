package kubectl

import (
	"os"
	"testing"


	"gitlab.com/digitalxero/simple-actions"
	"gitlab.com/digitalxero/simple-actions/log"
	"gitlab.com/digitalxero/simple-actions/term"
)

func TestPullAction(t *testing.T) {
	testCtx := actions.NewDefaultActionContext(term.NewLogger(os.Stderr, log.Level(3)), true)
	action := NewApplyAction("test", "bla", nil, false)
	if err := action.Execute(testCtx); err != nil {
		t.Fatal(err)
	}
}
